# Backup files to Google Drive

## Scenario 1. - The MSSQL database is running inside a docker container on Ubuntu Server.

### Installation

1. Install **Docker**. [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)

2. Install **Docker Compose** (optional). [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

3. Install **MSSQL server** (sample).
```
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=<!--password-->' -p 1433:1433 -v database:/var/opt/mssql -d --name mssql mcr.microsoft.com/mssql/server:2017-latest-ubuntu
```

*Notes:*
- Replace `<!--password-->` with your password.
- `-p` parameter will expose the TCP port 1433.
- `-v` parameter will link the `database` folder on the host machine with the `mssql` folder inside the container.
- `--name` specifies the name of the container

4. Install **curl**. We'll need this to install the next application.
```
sudo apt-get install curl
```

5. Install **rclone**.
```
curl https://rclone.org/install.sh | sudo bash
```

6. Configure **rclone**.
```
rclone config
```
*Configuration steps:*
- `No remotes found - make a new one` > **n** (New remote)
- `Name` > **google-drive**
- `Choose a number from below, or type in your own value` > **13** (Google Drive)
- `Google Application Client Id` > *Press Enter*
- `Google Application Client Secret` > *Press Enter*
- `Scope that rclone should use when requesting access from drive` > **1** (Full access)
- `ID of the root folder` > *Press Enter*
- `Service Account Credentials JSON file path` > *Press Enter*
- `Edit advanced config?` > *Press Enter* (No)
- `Use auto config?` > **n** (No)
- Open the generated link on your browser, and paste the verification code to the console.
- `Configure this as a team drive?` > *Press Enter* (No)
- `Confirm the generated configuration` > *Press Enter* (Yes this is OK)
- `Exit the configuration script` > **q**

7. Create a `make-backup.sh` script file.
```
#!/bin/sh

CONTAINER='<!--docker_container_id_or_name-->'
USER='<!--database_username-->'
PASS='<!--database_password-->'
DB='<!--database_name-->'
FILE="$DB ($(date +%Y_%m_%d)).bak"

docker exec -it $CONTAINER /opt/mssql-tools/bin/sqlcmd -S . -U $USER -P $PASS -Q "BACKUP DATABASE [$DB] TO DISK = '/var/opt/mssql/backup/$FILE'"
```
8. Make the `make-backup.sh` script file executable.
```
chmod +x ./make-backup.sh
```

9. Create a `sync-backup.sh` script file.
```
#!/bin/sh

SOURCE='<!--local_folder-->'
DEST='<!--google_drive_folder-->'

/usr/bin/rclone copy --update --verbose --transfers 30 --checkers 8 --contimeout 60s --timeout 300s --retries 3 --low-level-retries 10 --stats 1s $SOURCE "google-drive:$DEST"
```

*Notes:*
- **copy**: Copy the files from the local computer to the remote storage, skipping over files that are already present on the remote storage.
- **--update**: Skip any files that are on the remote storage that have a modified time that is newer than the file on the local computer.
- **--verbose**: Gives information about every file that is transferred.
- **--transfers 30**: This sets the number of files to copy in parallel.
- **--checkers 8**: How many “checkers” to run in parallel. Checkers monitor the transfers that are in progress.
- **--contimeout 60s**: The connection timeout. It sets the time that rclone will try to make a connection to the remote storage.
- **--timeout 300s**: If a transfer becomes idle for this amount of time, it is considered broken and is disconnected.
- **--retries 3**: If there are this many errors, the entire copy action will be restarted.
- **--low-level-retries 10**: A low-level retry tries to repeat one failing operation, such as a single HTTP request. This value sets the limit for the number of retries.
- **--stats 1s**: rclone can provide statistics on the transferred files. This sets the frequency of update of the statistics to one second.

10. Make the `sync-backup.sh` script file executable.
```
chmod +x ./sync-backup.sh
```

11. Create a `google-backup.sh` script file in `/etc/cron.daily/` folder.
```
#!/bin/sh

MAKE_PATH='<!--path_to_make_script-->'
SYNC_PATH='<!--path_to_sync_script-->'

. $MAKE_PATH && . $SYNC_PATH
```

12. Make the `google-backup.sh` script file executable.
```
chmod +x ./google-backup.sh
```